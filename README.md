##Changes

* Erased gradle because maven was asked.
* CarsAPI was not intended to be here. Deleted.
* Steps combined in bad scenarios, separated positive from negative.
* Added missing testrunner params.
* Created gitlab repo.
* Deleted github folders.
* As there are no more cases I won't use hooks. Unnecessary boilerplate.
* Added HTML reports and artifact into gitlab.
* Gave the folders some order.
* Changed Step definitions names, added a given case.
* Step definitions changed to declarative voice. Deleted unnecessary steps.
* Added the artifact to the pipelines (HTML reports).


#How to run

##From the command line
Write "mvn verify" and you are done.

##From the pipeline
You can use the "run pipeline" button in gitlab. You can override variables if you want to change cucumber's tags. As there weren't extra tests I did not add another tag.

##From the IDE
You can run the TestRunner class. You may find an intellij issue where you have to change the configuration to run the tests using the IDE instead of another option.