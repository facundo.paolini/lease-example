package runner;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        plugin = {"pretty","html:reports/MyLovelyReport.html"},
        features = "src/test/resources/features",
        glue = "stepdefinitions",
        tags = "@Search"
)
public class TestRunner {}
