package stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import net.serenitybdd.rest.SerenityRest;
import org.junit.Assert;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.equalTo;

public class SearchStepDefinitions {

    private Response response;
    private RequestSpecification request;

    @Given("the endpoint {string} is called")
    public void theEndpointIsCalled(String endpoint) {
        request = SerenityRest.given().baseUri(endpoint).contentType(ContentType.JSON);
    }

    @Then("status code is {int}")
    public void statusCodeIs(int statusCode) {
        restAssuredThat(response -> response.statusCode(statusCode));
    }

    @When("an item is looked for {string}")
    public void anItemIsLookedFor(String item) {
        response = request.when().get(item);
    }

    @And("results for {string} are displayed")
    public void resultsForAreDisplayed(String item) {
        Assert.assertTrue(response.getBody().asString().contains(item));
    }

    @And("results are not found")
    public void resultsForAreNotFound() {
        response.then().assertThat().body("detail.error", equalTo(true));
    }
}
