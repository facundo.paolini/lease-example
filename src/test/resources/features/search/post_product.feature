@Search
Feature: Search for the product

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/test/{product} for getting the products.
### Available products: "apple", "mango", "tofu", "water"
### Prepare Positive and negative scenarios

  @Positive
  Scenario Outline: Validate correct api call for a fruit
    Given the endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/" is called
    When an item is looked for <item>
    Then status code is <statusCode>
    And results for <item> are displayed
    Examples:
      | item    | statusCode |
      | "apple" | 200        |
      | "mango" | 200        |
      | "tofu"  | 200        |
      | "water" | 200        |


  @Negative
  Scenario: Validate correct call (or error) for the car item
    Given the endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/" is called
    When an item is looked for "car"
    Then status code is 404
    And results are not found

